-- Your SQL goes here
CREATE TABLE bans (
    id SERIAL PRIMARY KEY,
    message_id INTEGER,
    ip_addr VARCHAR(48),
    reason VARCHAR(16),
    expires TIMESTAMP
)