# About
Starway - chat that uses TCP to communicate, Rust to work and json to speak.
[![Build Status](https://travis-ci.org/starfear/starway.svg?branch=master)](https://travis-ci.org/starfear/starway)

# Documentation
1. [About request and response](https://github.com/starfear/starway/blob/master/docs/jsonbody.md)
2. [Rooms module](https://github.com/starfear/starway/blob/master/docs/rooms.md)


# Message examples
Create new room:
```json
{
	"id":     1,
	"action": "room_create",
	"data":   {
		"room_name": "my_room"
	}
}
```
should return:
```json
{
	"err":    null,
	"code":   "success",
	"action": "room_create",
	"id":	  1,
	"data":   {
		"room_name": "my_room"
	}
}
```
or if already exists:
```json
{
	"action": "room_create",
	"err":    true,
	"code":   "already_exists",
	"id":	  1,
	"data":   {
		"room_name": "my_room"
	}
}
```

# License
Starway is distributed under GNU GPL v3