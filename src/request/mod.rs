#[derive(Serialize, Deserialize)]
pub struct Request {
    pub id:     Option<i32>,
    pub action: String,
    pub data:   Option<serde_json::Value>
}

impl Request {
    pub fn new(id: Option<i32>, action: &str, data: Option<serde_json::Value>) -> Request {
        Request {
            id:     id,
            action: action.to_string(),
            data:   data
        }
    }
}
