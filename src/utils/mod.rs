pub fn serde_get_string(data: &serde_json::Value, key: &str) -> Option<String> {
    match data.get(key) {
        Some(v) => Some(v.as_str().unwrap().to_string()),
        None    => None
    }
}
