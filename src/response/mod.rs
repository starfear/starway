use {
    std::{net::TcpStream, io::Write },
    serde_json::json,

    std::collections::BTreeMap
};

#[derive(Serialize, Deserialize)]
pub struct Response {
    id:     Option<i32>,
    err:    Option<bool>,
    code:   String,
    action: Option<String>,
    data:   Option<serde_json::Value>
}

impl Response {
    pub fn new(id: Option<i32>, code: &str, action: Option<String>, data: Option<serde_json::Value>) -> Response {
        // set err to true if code is not success
        let err = { if code == "success" { None } else { Some(true) } };

        Response {
            id:     id,
            err:    err,
            code:   String::from(code),
            action: action,
            data:   data
        }
    }

    pub fn from_json(val: serde_json::Value) -> Response {
        serde_json::from_value(val).unwrap()
    }

    pub fn as_string(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        self.as_string().as_bytes().to_vec()
    }

    pub fn write(&self, mut stream: &TcpStream) {
        stream.write(&self.as_bytes()).expect("Unable to write to stream");
    }

    pub fn success(id: Option<i32>, action: &str) -> Response {
        Response::new(id, "success", Some(String::from(action)), None)
    }

    pub fn success_data(id: Option<i32>, action: &str, data: serde_json::Value) -> Response {
        Response::new(id, "success", Some(String::from(action)), Some(data))
    }

    pub fn err(id: Option<i32>, code: &str) -> Response {
        Response::new(id, code, None, None)
    }

    pub fn err_action(id: Option<i32>, action: &str, code: &str) -> Response {
        Response::new(id, code, Some(String::from(action)), None)
    }

    pub fn err_action_data(id: Option<i32>, action: &str, code: &str, data: serde_json::Value) -> Response {
        Response::new(id, code, Some(String::from(action)), Some(data))
    }

    pub fn argument_missing(id: Option<i32>, action: &str) -> Response {
        Response::new(id, "argument_missing", Some(String::from(action)), None)
    }

    pub fn panic() -> Response {
        Response::new(None, "panic", None, None)
    }

    pub fn from_syntax_error(id: Option<i32>, action: &str, e: validator::ValidationErrors) -> Response {
        // get fields
        let fields = e.field_errors();

        // create map of fields
        let mut fields_map: BTreeMap<String, BTreeMap<String, String>> = BTreeMap::new();

        // iterate each
        for (name, errors) in fields {
            // create new map
            let mut errors_map: BTreeMap<String, String> = BTreeMap::new();

            // iterate errors
            for error in errors {
                // get error message or code
                let message: String = match &error.message {
                    Some(v) => v.to_string(),
                    None => error.code.to_string()
                };

                // insert error in errors
                errors_map.insert(error.code.to_string(), message);
            }

            // insert field errors in anothed fields map
            fields_map.insert(name.to_string(), errors_map);
        }

        // return
        Response::new(id, "syntax_error", Some(String::from(action)), Some( json!({ "errors": fields_map }) ))
    }
}