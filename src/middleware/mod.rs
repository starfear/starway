use {
    std::{
        net::TcpStream,
    },

    diesel::MysqlConnection,

    super::{
        db::models::*,
        response::Response
    }
};

pub fn ip_banned(connection: &MysqlConnection, id: Option<i32>, action: Option<String>, stream: &TcpStream) -> bool {
    let peer_addr = stream.peer_addr().unwrap();
    let ip = peer_addr.ip().to_string();

    // check if user is banned
    let bans: Vec<Ban> = Ban::find_active(connection, &ip);

    // if there is any ban then send errbanned and return true
    if bans.len() != 0 {
        Response::new(id, "ERRBANNED", action, Some(serde_json::json!({
            "tip": "You are not really banned, Check #Manual::WhatIsTheBan."
        }))).write(stream);

        return true;
    }

    false
}