use {
    std::{
        thread,
        io::{Read},
        collections::{HashMap},
        net::{TcpStream, TcpListener },
        sync::{Arc, Mutex, MutexGuard }
    },
    super::response::Response,
    super::request::Request,
    serde_json::{json},
};

const CONST_BUFFER_LENGTH: usize = 1024;

pub type Action           = fn(&TcpStream, i32, String, Request);
pub type StreamMap        = HashMap<i32,    TcpStream>;
pub type ActionMap        = HashMap<String, Action>;
pub type CustomUnregister = fn(&mut MutexGuard<StreamMap>, i32);

pub struct TcpBridge {
    pub client_id: Arc<Mutex<i32>>,
    pub addr:      Arc<Mutex<String>>,
    pub streams:   Arc<Mutex<StreamMap>>,
    pub actions:   Arc<Mutex<ActionMap>>,

    pub custom_unregister: Arc<Mutex<CustomUnregister>>
}

impl TcpBridge {
    pub fn new() -> TcpBridge {
        TcpBridge {
            client_id: Arc::new(Mutex::new(0)),
            addr:      Arc::new(Mutex::new(String::new())),
            streams:   Arc::new(Mutex::new(HashMap::new())),
            actions:   Arc::new(Mutex::new(HashMap::new())),
            custom_unregister: Arc::new(Mutex::new(|_, _| {}))
        }
    }

    pub fn new_with_addr(addr: &str) -> TcpBridge {
        TcpBridge {
            client_id: Arc::new(Mutex::new(0)),
            addr:      Arc::new(Mutex::new(String::from(addr))),
            streams:   Arc::new(Mutex::new(HashMap::new())),
            actions:   Arc::new(Mutex::new(HashMap::new())),
            custom_unregister: Arc::new(Mutex::new(|_, _| {}))
        }
    }

    // addr setter
    pub fn set_addr(&mut self, new_addr: &str) {
        // lock address container
        let mut addr = self.addr.lock().unwrap();

        // clear address
        addr.clear();

        // push new addr
        addr.push_str(new_addr);
    }

    pub fn listen(&self) -> std::io::Result<()> {
        // lock address container
        let addr       = self.addr.lock().unwrap();

        // thread prepare, cloning mutex vars
        let streams    = self.streams.clone();
        let client_id  = self.client_id.clone();
        let actions    = self.actions.clone();
        let unregister = self.custom_unregister.clone();
        let listener = TcpListener::bind(&*addr)?;

        // create new incoming stream for TcpListener        
        thread::spawn(move || {
            for stream in listener.incoming() {
                match stream {
                    Ok(stream) => {
                        // lock stream list and client_id
                        let mut _streams   = streams  .lock().unwrap();
                        let mut client_id  = client_id.lock().unwrap();

                        // register stream
                        register_stream(&mut _streams, &mut client_id, &stream);

                        // create stream listener
                        stream_listener(*client_id, &stream, actions.clone(), streams.clone(), unregister.clone());

                        // drop ownership of streams and client_id
                        drop(_streams);
                        drop(client_id);
                    },

                    Err(_) => {
                        // NOTE: Should drop stream
                    }
                }
            }
        });

        Ok(())
    }

    pub fn custom_unregister_set(&mut self, f: CustomUnregister) {
        *self.custom_unregister.lock().unwrap() = f;
    }

    pub fn action_add(&mut self, name: &str, f: Action) {
        self.actions.lock().unwrap()
            .insert(name.to_string(), f);
    }

    pub fn get_addr(&self) -> String {
        self.addr.lock().unwrap().clone()
    }

    pub fn get_online(&self) -> usize {
        self.streams.lock().unwrap().len()
    }
}

// utils
pub fn action_emit(stream: &TcpStream, client_id: i32, request: Request, acts: Arc<Mutex<ActionMap>>) {
    // create clone of map
    let c = acts.clone();

    // lock actions map
    let actions = c.lock().unwrap();

    // find, if not exists redirect to not_found
    match actions.get(&request.action) {
        Some(v) => {
            v(stream, client_id, request.action.clone(), request)
        },
        None    => {
            if request.action == "not_found" {
                panic!("not_found action not found.");
            } else {
                drop(actions);
                // create new request with same id, action not_found and data with action_name
                let not_found_request = Request::new(request.id, "not_found", Some(json!({"action": request.action})));
                
                // return it
                return action_emit(stream, client_id, not_found_request, acts)
            }
        }
    };
}

fn register_stream(streams: &mut MutexGuard<StreamMap>, client_id: &mut std::sync::MutexGuard<'_, i32>, stream: &TcpStream) {
    **client_id += 1;

    streams.insert(**client_id, stream.try_clone().unwrap());

    println!("[{}] connected from {}", **client_id, stream.peer_addr().unwrap());
}

fn unregister_stream(streams: &mut MutexGuard<StreamMap>, client_id: i32) {
    streams.remove(&client_id);

    println!("[{}] disconnected", client_id);
}

fn stream_listener(client_id: i32, stream: &TcpStream, actions: Arc<Mutex<ActionMap>>, streams: Arc<Mutex<StreamMap>>, custom_unregister: Arc<Mutex<CustomUnregister>>) {
    // copy stream
    let mut stream = stream.try_clone().unwrap();

    // create listener thread
    thread::spawn(move || {
        // create buffer
        let mut buffer = [0 as u8; CONST_BUFFER_LENGTH];

        loop {
            match stream.read(&mut buffer) {
                Ok(size) => {
                    // if disconnected
                    if size == 0 {
                        let mut streams = streams.lock().unwrap();
                        // call global unregister
                        unregister_stream(&mut streams, client_id);

                        // lock and call user unregister
                        let func = custom_unregister.lock().unwrap();
                        func(&mut streams, client_id);

                        break;
                    }

                    // [u8] -> String
                    let message = String::from_utf8_lossy(&buffer)
                        .trim_matches(char::from(0))
                        .to_string()
                        .trim()
                        .to_string();

                    // clear buffer
                    buffer = [0 as u8; CONST_BUFFER_LENGTH];

                    // create request
                    let request: Result<Request, serde_json::Error> = serde_json::from_str(&message);

                    match request {
                        Ok(request) => {
                            action_emit(&stream, client_id, request, actions.clone())
                        },
                        Err(_) => {
                            Response::err(None, "json_invalid").write(&stream);
                        }
                    };
                },

                Err(_) => {
                    // timeout
                    let mut streams = streams.lock().unwrap();
                    unregister_stream(&mut streams, client_id);
                }
            }
        }
    });
}