// import lazy_static library
#[macro_use] extern crate lazy_static;

// import clap to parse cli argument
extern crate clap;

// import lib.rs
extern crate starwayd;

// use
use {
    // clap
    clap::{Arg, App},

    // diesel
    diesel::{MysqlConnection},
    
    // std
    std::{
        process::exit,
        sync::{Mutex, MutexGuard},
        net::{SocketAddr, TcpStream}
    },

    // starwayd
    self::{
        starwayd::{
            *,
            request::Request,
            response::Response,
            bridge::StreamMap,
            valider::*,
            utils::serde_get_string
        },
        db::models::*,
    },

    // validator
    validator::Validate,
    
    // serde
    serde_json::json
};

lazy_static! {
    // get the conenction
    static ref GLOBAL_CONNECTION: Mutex<MysqlConnection>   = Mutex::new(db::establish_connection());

    // create bridge
    static ref GLOBAL_BRIDGE:     Mutex<bridge::TcpBridge> = Mutex::new(bridge::TcpBridge::new());
}

fn main() {
    // initialize
    init();

    // parse cli argument
    let matches = App::new("Starway daemon")
                    .version(env!("CARGO_PKG_VERSION"))
                    .author("Universe Up")
                    .about("Starway - the stupid TCP chat.")
                    .arg(Arg::with_name("addr")
                        .short("a")
                        .long("addr")
                        .help("Sets an server address (ip:port)")
                        .takes_value(true)
                        .required(true)
                        .index(1)
                    )
                    .get_matches();

    // obtain the address
    let addr = matches.value_of("addr").expect("No addr found in arguments.");

    // check if address is valid using std::SocketAddr parser
    let _: SocketAddr = addr.parse().unwrap_or_else(|_| { 
        println!("Address {:?} is invalid, exiting...", addr);
        exit(1);
    });

    /* Startup [DB] */
    prepare_tables();

    /* Startup [TcpServer] */
    // lock bridge
    let mut bridge = GLOBAL_BRIDGE.lock().unwrap();

    // set new addr
    bridge.set_addr(addr);

    // ... actions and events
    bridge.custom_unregister_set(custom_unregister);
    bridge.action_add("get_online",  action_get_online);
    bridge.action_add("room_create", action_room_create);
    bridge.action_add("room_join",   action_room_join);
    bridge.action_add("room_write",  action_room_write);
    bridge.action_add("ban_me",      action_ban_me);

    // add action not_found
    bridge.action_add("not_found", |stream: &TcpStream, _client_id: i32, _action: String, request: Request| {
        // get the original action name
        let original_action: Option<String> = match request.data {
            Some(v) => {
                match serde_get_string(&v, "action") {
                    Some(v) => Some(v),
                    None    => None
                }
            },

            None    => None
        };

        return Response::new(request.id, "not_found", original_action, None).write(stream);
    });

    // listen
    bridge.listen().unwrap_or_else(|e| {
        println!("Error binding addr {}: {}", addr, e);
        exit(1);
    });

    // notify
    println!("New listener created at {}", bridge.get_addr());

    //drop
    drop(bridge);

    loop{ std::thread::sleep(std::time::Duration::from_millis(100)) }
}

fn prepare_tables() {
    // lock the db connection
    let connection = GLOBAL_CONNECTION.lock().unwrap();
    
    // Wipe rooms and their relationships to users
        Room::wipe(&connection);
    UserRoom::wipe(&connection);
}

// unregister
fn custom_unregister(_streams: &mut MutexGuard<StreamMap>, client_id: i32) {
    // lock the connection
    let connection = GLOBAL_CONNECTION.lock().unwrap();

    // delete all relations
    UserRoom::forget_user(&connection, client_id);
}

// actions...
fn action_ban_me(stream: &TcpStream, _client_id: i32, _action: String, _request: Request) {
    // get the peer addr
    let peer_addr = stream.peer_addr().unwrap();

    // obtain addr as string
    let addr = match peer_addr {
        SocketAddr::V4(v) => {v.ip().to_string()},
        SocketAddr::V6(v) => {v.ip().to_string()}
    };

    // lock db
    let connection = GLOBAL_CONNECTION.lock().unwrap();

    // create new ban
    let ban = Ban::create(&connection, &addr, 800, 0, "R");

    // print ban
    println!("{:?}", ban);
}

fn action_room_write(stream: &TcpStream, client_id: i32, action: String, request: Request) {
    // lock the connection
    let c = GLOBAL_CONNECTION.lock().unwrap();

    // check if user is banned
    if middleware::ip_banned(&c, request.id, Some(request.action), stream) == true {
        return;
    };

    // load data
    let data = match request.data {
        Some(d) => d,
        None    => {return Response::err_action(request.id, &action, "data_required").write(stream)}
    };

    // load data to model
    let mdl: RoomWriteData = match serde_json::from_value(data) {
        Ok(v) => v,
        Err(_) => {
            return Response::argument_missing(request.id, &action).write(stream)
        }
    };

    // validate
    match mdl.validate() {
        Ok(_) => {},
        Err(e) => {
            return Response::from_syntax_error(request.id, &action, e).write(stream)
        }
    }

    // get room
    let room = match Room::get_by_name(&c, &mdl.room_name) {
        Some(v) => v,
        None    => {
            return Response::err_action_data(request.id, &action, "room_not_found", json!({
                "room_name": mdl.room_name
            })).write(stream)
        }
    };

    // check if user is a member of room
    if room.member_exists(&c, client_id) == false {
        return Response::err_action_data(request.id, &action, "not_a_member", json!({
            "room_name": mdl.room_name
        })).write(stream)
    }

    // get msg type
    let msg_type: String = match mdl.msg_type {
        Some(v) => v,
        None    => String::from("raw")
    };

    // get members
    let members = room.member_list(&c);

    // create message
    let message = Response::from_json(json!({
        "err":  null,
        "code": "room_new_message",
        "data": {
            "room_name": mdl.room_name,
            "message":   mdl.message,
            "msg_type":  msg_type
        }
    }));

    // lock the bridge and streams
    let bridge  = GLOBAL_BRIDGE .lock().unwrap();
    let streams = bridge.streams.lock().unwrap();

    // iterate each and write
    for member in members {
        // get correct stream
        let _stream = match streams.get(&member) {
            // if found
            Some(v) => message.write(&v),

            // if not found
            None    => {
                Response::panic().write(stream);
                panic!(format!("no stream found for user {} while iterating list in room_write.", member));
            }
        };
    };

    Response::success_data(request.id, &action, json!({"room_name": mdl.room_name})).write(stream);
}

fn action_room_join(stream: &TcpStream, client_id: i32, action: String, request: Request) {
    // load data
    let data = match request.data {
        Some(d) => d,
        None    => {return Response::err_action(request.id, &action, "data_required").write(stream)}
    };

    // get room_name
    let room_name = match serde_get_string(&data, "room_name") {
        Some(v) => v.to_string(),
        None => {
            return
        }
    };

    // lock the connection
    let c = GLOBAL_CONNECTION.lock().unwrap();

    // get room
    let room = match Room::get_by_name(&c, &room_name) {
        Some(v) => v,
        None    => {
            return Response::err_action_data(request.id, &action, "room_not_found", json!({
                "room_name": room_name
            })).write(stream)
        }
    };

    // check if user is already a member of room
    match room.member_exists(&c, client_id) {
        false => {
            // if ok
            room.member_add(&c, client_id);
        
            Response::success_data(request.id, &action, json!({"room_name": room_name})).write(stream)
        },

        true => {
            Response::err_action_data(request.id, &action, "already_joined", json!({
                "room_name": room_name
            })).write(stream)
        }
    }
}

fn action_room_create(stream: &TcpStream, client_id: i32, action: String, request: Request) {
    // load data
    let data = match request.data {
        Some(d) => d,
        None    => {return Response::err_action(request.id, &action, "data_required").write(stream)}
    };

    // load data to model
    let data: RoomCreateData = match serde_json::from_value(data) {
        Ok(v) => v,
        Err(_) => {
            return Response::argument_missing(request.id, &action).write(stream)
        }
    };

    // validate
    match data.validate() {
        Ok(_) => {},
        Err(e) => {
            return Response::from_syntax_error(request.id, &action, e).write(stream)
        }
    }

    // lock the connection
    let c = GLOBAL_CONNECTION.lock().unwrap();

    // prevent duplication
    if Room::exists(&c, &data.room_name) {
        return Response::err_action_data(request.id, &action, "room_exists", json!({"room_name": &data.room_name})).write(stream);
    }

    // create room and add user there
    let room = Room::create(&c, &data.room_name, "Not specified", 32);
    room.member_add(&c, client_id);

    // send ok
    Response::success(request.id, &action).write(stream);
}

fn action_get_online(stream: &TcpStream, _client_id: i32, action: String, request: Request) {
    // lock bridge
    let bridge = GLOBAL_BRIDGE.lock().unwrap();

    // get online
    let online = bridge.get_online();

    // write
    Response::success_data(request.id, &action, json!({"online": online})).write(stream);
}
