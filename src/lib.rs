// import lazy_static library
#[macro_use] extern crate lazy_static;

// .env
extern crate dotenv;

// timestamps
extern crate chrono;
extern crate time;

// validator
#[macro_use]
extern crate validator_derive;
extern crate validator;

// serde
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

// use diesel ORM library to deal with mysql db
#[macro_use] extern crate diesel;

// include utils module
pub mod utils;

// include request module
pub mod request;

// include response module
pub mod response;

// include high level lib
pub mod db;

// include high level socket lib 
pub mod bridge;

// include models for validator
pub mod valider;

// include midlewares functions
pub mod middleware;

// global init
pub fn init() {
    // load environment settings
    dotenv::dotenv().ok();
}
