use {
    chrono::prelude::*,

    diesel::{
        prelude::*,

        // dsls
        dsl::{
            select,
            exists,
            delete,
        }
    },
    super::schema::*
};

// Model: Ban
#[derive(Debug, Clone, Queryable)]
pub struct Ban {
    pub id:          i32,
    pub message_id:  i32,
    pub ip_addr:     String,
    pub reason:      String,
    pub expires:     NaiveDateTime
}

#[derive(Insertable)]
#[table_name="bans"]
pub struct NewBan<'a> {
    pub message_id:  i32,
    pub ip_addr:     &'a str,
    pub reason:      &'a str,
    pub expires:     NaiveDateTime
}

impl Ban {
    pub fn create<'a>(connection: &MysqlConnection, ip_addr: &str, duration: i64, message_id: i32, reason: &str) -> Ban {
        // create new model
        let new_ban = NewBan {
                message_id  : message_id,
                    expires : (Local::now() + time::Duration::minutes(duration)).naive_utc(),
                    ip_addr : ip_addr,
                     reason : reason
        };

        // save new model
        diesel::insert_into(bans::table)
         .values(&new_ban)
         .execute(connection)
         .expect("Error saving new room");

        // get real model and return
        bans::table.order(bans::id.desc()).first(connection).unwrap()
    }

    pub fn find_active<'a>(connection: &MysqlConnection, ip: &str) -> Vec<Ban> {
        // initialize future vector of bans
        let mut bans_vec: Vec<Ban> = Vec::new();

        // get current timestamp
        let now = Local::now() .naive_utc();

        // perform query
        let results = bans::table                        // SELECT * FROM `bans`
                      .filter( bans::ip_addr .eq(ip)  )  // `peer_addr` = ${ip}
                      .filter( bans::expires .gt(now) )  // `expires`   > CURRENT_TIMESTAMP
                      .load::<Ban>(connection).unwrap();

        // load resultrs to vec
        for result in results {
            bans_vec.push(result);
        }

        // return vector
        bans_vec
    }
}

// Model: Room
#[derive(Debug, Clone, Queryable)]
pub struct Room {
    pub id:          i32,
    pub name:        String,
    pub description: String,
    pub max_members: i32
}

#[derive(Insertable)]
#[table_name="rooms"]
pub struct NewRoom<'a> {
    pub name:        &'a str,
    pub description: &'a str,
    pub max_members: i32
}

impl Room {
    pub fn create<'a>(connection: &MysqlConnection, name: &'a str, description: &'a str, max_members: i32) -> Room {
        // create new model
        let new_room = NewRoom {
            name:        name,
            description: description,
            max_members: max_members
        };

        // save new model
        diesel::insert_into(rooms::table)
         .values(&new_room)
         .execute(connection)
         .expect("Error saving new room");

        // get real model and return
        rooms::table.order(rooms::id.desc()).first(connection).unwrap()
    }

    pub fn wipe<'a>(connection: &MysqlConnection) {
        // remove all rooms
        delete(rooms::table).execute(connection).expect("Unable to wipe rooms table");
    }

    pub fn member_list<'a>(&self, connection: &MysqlConnection) -> Vec<i32> {
        UserRoom::users(connection, self.id)
    }

    pub fn member_exists<'a>(&self, connection: &MysqlConnection, user_id: i32) -> bool {
        UserRoom::exists(connection, user_id, self.id)
    }

    pub fn get_by_name<'a>(connection: &MysqlConnection, name: &str) -> Option<Room> {
        use rooms::table;

        // query
        let result = table
            .filter(rooms::name.eq(name))
            .limit(1)
            .load::<Room>(connection)
            .expect("Unable to load room");
    
        match result.get(0) {
            Some(v) => Some(v.clone()),
            None    => None
        }
    }

    pub fn member_add<'a>(&self, connection: &MysqlConnection, user_id: i32) -> UserRoom {
        UserRoom::create(connection, user_id, self.id)
    }

    pub fn exists<'a>(connection: &MysqlConnection, name: &'a str) -> bool {
        // create query and return result
        select(exists(rooms::table.filter(rooms::name.eq(name))))
            .get_result(connection)
            .unwrap()
    }

    pub fn not_exists<'a>(connection: &MysqlConnection, name: &'a str) -> bool {
        ! Room::exists(connection, name)
    }
}

#[derive(Queryable)]
pub struct UserRoom {
    pub id:      i32,
    pub room_id: i32,
    pub user_id: i32
}

#[derive(Insertable)]
#[table_name="user_room"]
pub struct NewUserRoom {
    pub room_id: i32,
    pub user_id: i32
}

impl UserRoom {
    pub fn create<'a>(connection: &MysqlConnection, user_id: i32, room_id: i32) -> UserRoom {
        // create save model
        let new_rel = NewUserRoom {
            room_id: room_id,
            user_id: user_id
        };

        // save new model
        diesel::insert_into(user_room::table)
         .values(&new_rel)
         .execute(connection)
         .expect("Error saving new room");

        // get real model and return
        user_room::table.order(user_room::id.desc()).first(connection).unwrap()
    }

    pub fn wipe<'a>(connection: &MysqlConnection) {
        delete(user_room::table).execute(connection).expect("Unable to wipe user_room table");
    }

    pub fn exists<'a>(connection: &MysqlConnection, user_id: i32, room_id: i32) -> bool {
        select(exists(user_room::table                      // select exists
                .filter(user_room::user_id .eq(user_id))    // where `user_id`={user_id}
                .filter(user_room::room_id .eq(room_id))    // where `room_id`={room_id}
        )).get_result(connection).unwrap()                  // execute and return
    }

    pub fn forget_user<'a>(connection: &MysqlConnection, _user_id: i32) {
        use {
            user_room::{
                table,
                user_id
            }
        };

        // delete
        let _ = delete(table.filter(user_id.eq(_user_id))).execute(connection);
    }

    pub fn users<'a>(connection: &MysqlConnection, room_id: i32) -> Vec<i32> {
        // create vec
        let mut users = Vec::new();

        // load result
        let result = user_room::table
            .select(user_room::user_id)
            .filter(user_room::room_id .eq(room_id))
            .load::<i32>(connection)
            .expect("unable to get users of room");

        for result in result {
            users.push(result);
        }

        users
    }
}