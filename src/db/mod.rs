use {diesel::prelude::*, std::env };

// include schema and models
pub mod schema;
pub mod models;

pub fn establish_connection() -> MysqlConnection {
    // get database url
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL is not set.");

    // establish connection and return it
    MysqlConnection::establish(&database_url)
        .expect(&format!("Unable to connect to {}", database_url))
}
