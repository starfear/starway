table! {
    bans (id) {
        id -> Integer,
        message_id -> Integer,
        ip_addr -> Text,
        reason -> Text,
        expires -> Timestamp,
    }
}

table! {
    rooms (id) {
        id -> Integer,
        name -> Text,
        description -> Text,
        max_members -> Integer,
    }
}

table! {
    user_room (id) {
        id -> Integer,
        room_id -> Integer,
        user_id -> Integer,
    }
}

allow_tables_to_appear_in_same_query!(
    bans,
    rooms,
    user_room,
);