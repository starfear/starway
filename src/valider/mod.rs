use {
    validator::{ Validate },
    regex::Regex,
};

lazy_static! {
    static ref REGEX_ROOM_NAME: Regex = Regex::new("^[A-Za-z][A-Za-z0-9_]+$").unwrap();
}

#[derive(Debug, Validate, Deserialize)]
pub struct RoomCreateData {
    #[validate(length(min = 3, max = 25, message = "4-25"))]
    #[validate(regex( path = "REGEX_ROOM_NAME", message = "regex_room_name"))]
    pub room_name: String,
}

#[derive(Debug, Validate, Deserialize)]
pub struct RoomWriteData {
    #[validate(length(min = 3, max = 25, message = "4-25"))]
    #[validate(regex( path = "REGEX_ROOM_NAME", message = "regex_room_name"))]
    pub room_name: String,

    #[validate(length(min = 1, max = 1024))]
    pub message: String,

    #[validate(length(min = 1, max = 32, message = "1-32"))]
    pub msg_type: Option<String>
}