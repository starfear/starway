# Room module
## Actions
### "room_create"
#### Request
* room_name - Name of future room (length: 3 to 25 characters, should also match regex: ^[A-Za-z][A-Za-z0-9_]+$)

#### Response types
##### "success"
###### Description
Room is created successfuly, user should auto join in room.
###### Fields
* room_name - Name of room

##### "room_exists"
###### Description
Room is not create because already exists.
###### Fields
* room_name - Name of room

---

### "room_join"
#### Request
* room_name - Name of room you want to join.

#### Response types
##### "success"
###### Description
You have successfuly joined the room.
###### Fields
* room_name - Name of room

##### "room_not_found"
###### Description
Room you want to join is not found.
###### Fields
* room_name - Name of room

##### "already_joined"
###### Description
You have already joined the room.
###### Fields
* room_name - Name of room

---

### "room_write"
#### Request
* room_name         - Room where you want to write.
* message           - Message body (length 1 to 1024)
* msg_type **optional** - type of message (if null: raw) 

#### Response types
##### "success"
###### Description
You have successfuly written message.
###### Fields
* room_name   -- Name of room where you wrote message

##### "room_not_found"
###### Description
Room does not exist.
###### Fields
* room_name   - Name of room

##### "not_a_member"
###### Description
You are unable to write to room beacause you are not a member of room.
###### Fields
* room_name   - Name of room