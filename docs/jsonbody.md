# Json Body
## Request
> Note: Request is that user sends to server.

Request must always have *action* paramater.
It tells server what user want.

for example:
```javascript
{
    "action": "get_online"
}
```
tells user want to get current online on server.

## Response
> Note: Response is that server sends to user.

After request server should send response (null fields are not shown):
```javascript
{
    "code":   "success",
    "action": "get_online",
    "data":   {             /* for response data fields contains oprional fields */
        "online": 1
    }
}
```

## Arguments
Request json can also contain *data* object, it allows to pass arguments as object properties:

for example lets create new room:
```javascript
{
    "action": "room_create",
    "data":   {
        "room_name": "my_super_room"
    }
}
```

Server should also send response(null fields are not shown):
```javascript
{
    "code":   "success",
    "action": "room_create",
    "data": {
        "room_name": "my_super_room"
    }
}
```

## Errors
Error response looks like(null fields are not shown):
```javascript
{
    "err":    true,          /* tells if response is an error                          */
    "code":   "room_exists", /* tells code of error (anything except "success")     */
    "action": "room_create", /* tells what action has just throwen an error         */
    "data":   {
        "room_name": "my_super_room"
    }
}
```

## Request To Response relationship recognization
If you want to find the relationship between request and response, you can simple add "id" field to request
```javascript
{
    "id":     1,
    "action": "get_online"
}
```

So, response will look like:
```javascript
{
    "id":     1,
    "code":   "success"
    "action": "get_online",
    "data":   {
        "online": 1
    }
}
```